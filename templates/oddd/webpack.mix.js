let mix = require('laravel-mix');
mix.js('src/oddd.js', 'dist/')
  .sass('src/oddd.scss', 'dist/')
  .vue({version:2});
